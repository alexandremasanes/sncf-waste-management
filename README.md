# Application Waste Management

Auteur : `Alexandre Masanes` , `Diane Mansuy`

Date de début : `31/01/2018`

## Code source

Projet réalisé avec `Maven` (outils de gestion et d'automatisation de production).

    -> configuration dans le pom.xml à la racine

Projet en `Java 8`

Autres languages : `HTML5`, `CSS3`, `TypeScript` 

base de données en Posgresql : `waste-management`

Framework : 
        
                - pour le côté web mvc : spring-web
                - pour la persistance des données : hibernate
                - pour le côté clients : angular

Trouver dans le dossier `client` le code type script du
framework `Angular` 
 
Trouver dans le dossier `server` le code java du framework
`Spring` 
    
Trouver dans le dossier `etc` le script de la `base de
données` 

## Utilisation

1- télécharger le projet ou le cloner avec la commande `git clone git@gitlab.com:aleksbenmaza/sncf-waste-management.git

2- avoir `docker` et `docker-compose` d'installés et un navigateur supportant `HTML 5`  

3- se placer dans le répertoire deployment  
 
4- lancer la commande `docker-compose up`

5- Ouvrir son navigateur à l'adresse suivante : http://172.28.1.4/

