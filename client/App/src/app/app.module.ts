import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RootComponent } from './component/root/root.component';
import { LoginFormComponent } from './component/root/login-form/login-form.component';
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { TokenService } from "./service/token.service";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule, Routes } from "@angular/router";
import { MainPanelComponent } from './component/root/main-panel/main-panel.component';
import {ThirdPartyIndexComponent} from "./component/root/main-panel/third-party/routable/index/third-party-index.component";
import {ManagerIndexComponent} from "./component/root/main-panel/manager/routable/index/manager-index.component";
import {ManagerNavbarComponent} from "./component/root/main-panel/manager/navbar/manager-navbar.component";
import {ThirdPartyNavbarComponent} from "./component/root/main-panel/third-party/navbar/third-party-navbar.component";
import {ThirdPartyUserComponent} from "./component/root/main-panel/third-party/routable/user/third-party-user.component";
import {WebStorageModule} from "../../../../server/src/main/resources/node_modules/ngx-store/src/ngx-store";



const routes: Routes = [
  { path: 'third-party', component: ThirdPartyIndexComponent },
  { path: 'third-party/account', component:ThirdPartyUserComponent },
  {
    path: 'manager',
    component: ManagerIndexComponent,
    data: { title: 'Heroes List' }
  }
];

@NgModule({
  declarations: [
    RootComponent,
    LoginFormComponent,
    ManagerIndexComponent,
    ThirdPartyIndexComponent,
    MainPanelComponent,
    ManagerNavbarComponent,
    ThirdPartyNavbarComponent,
    ManagerIndexComponent,
    ThirdPartyIndexComponent,
    ThirdPartyUserComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      routes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    WebStorageModule
  ],
  providers: [
    TokenService
  ],
  bootstrap: [
    RootComponent
  ]
})
export class AppModule { }
