import {Component, OnInit, Input} from '@angular/core';
import {User} from "../../../../model/input/user/User";

@Component({
  selector: 'main-panel',
  templateUrl: 'main-panel.component.html',
  styleUrls: ['main-panel.component.css'],
})
export class MainPanelComponent implements OnInit {

  @Input()
  user: User;

  constructor() { }

  ngOnInit() {
  }

}
