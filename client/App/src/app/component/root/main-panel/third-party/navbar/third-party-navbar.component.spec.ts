import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import ThirdPartyNavbarComponent from './third-party-navbar.component';

describe('ThirdPartyNavbarComponent', () => {
  let component: ThirdPartyNavbarComponent;
  let fixture: ComponentFixture<ThirdPartyNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThirdPartyNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdPartyNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should createStock', () => {
    expect(component).toBeTruthy();
  });
});
