import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import ThirdPartyUserComponent from './third-party-user.component.ts';

describe('ThirdPartyUserComponent', () => {
  let component: ThirdPartyUserComponent;
  let fixture: ComponentFixture<ThirdPartyUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThirdPartyUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdPartyUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should createStock', () => {
    expect(component).toBeTruthy();
  });
});
