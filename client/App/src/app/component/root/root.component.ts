import {Component, OnInit, ViewChild, Input, OnDestroy, HostListener} from '@angular/core';
import {LoginFormComponent} from "./login-form/login-form.component";
import {MainPanelComponent} from "./main-panel/main-panel.component";
import {Token} from "../../../model/input/Token";
import {Router} from "@angular/router";
import {LocalStorage} from "../../../../../../server/src/main/resources/node_modules/ngx-store/src/decorator/webstorage";

@Component({
  selector: 'root',
  templateUrl: 'root.component.html',
  styleUrls: ['root.component.css'],
})
export class RootComponent implements OnInit, OnDestroy {

  title: string = 'Waste Management';

  @LocalStorage()
  @Input()
  token: Token;

  @ViewChild(LoginFormComponent)
  private loginForm: LoginFormComponent;

  @ViewChild(MainPanelComponent)
  private mainPanel: MainPanelComponent;


  constructor(private router: Router) {
  }

  onTokenEvent(token: Token): void {
    let uri: string;

    this.token = token;

    if(this.router.url == '/') {
      switch(token.user.__TYPE__) {
        case 'Manager':
          uri = '/manager';
          break;
        case 'ThirdParty' :
          uri = '/third-party';
          break;
        default:
          throw new Error;
      }
      this.router.navigate([uri]);
    }
  }

  @HostListener('window:beforeunload')
  beforeUnload(): void {
    if(!this.loginForm.rembemberMe)
      this.token = null;
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }
}
