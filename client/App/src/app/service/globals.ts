/**
 * Created by alexandremasanes on 05/02/2018.
 */

export const API_SERVER: string = "localhost:8888";

export const PUBLIC_API_SUBDOMAIN: string = "p";

export const MANAGER_API_SUBDOMAIN: string = "m";

export const THIRD_PARTY_API_SUBDOMAIN: string = "tp";
