/**
 * Created by alexandremasanes on 29/03/2018.
 */

export interface CompanyCreation {

  name: string;

  sirenNumber: string;
}
