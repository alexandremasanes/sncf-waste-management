/**
 * Created by alexandremasanes on 02/02/2018.
 */

export interface TokenCreation{

  emailAddress: string;

  password: string;
}
