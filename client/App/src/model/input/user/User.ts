/**
 * Created by alexandremasanes on 04/02/2018.
 */
export interface User {

  __TYPE__: string;

  id: number;

  emailAddress: string;

  lastName: string;

  firstName: string;
}
