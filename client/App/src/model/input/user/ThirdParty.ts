import {User} from "./User";
import {Company} from "../Company";
/**
 * Created by alexandremasanes on 04/02/2018.
 */
export interface ThirdParty extends User {

  company: Company;
}
