
import {Material} from "./Material";

export enum Quality {
  BAD,
  AVERAGE,
  GOOD
}

export interface StockId {

  materialId: number;

  quality: Quality;
}

export interface Stock {

  material: Material;

  quality: Quality;

  quantity: number;

  projectIdQuantities: [{[key: number]: number}];
}
