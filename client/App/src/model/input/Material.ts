/**
 * Created by alexandremasanes on 29/03/2018.
 */
import {Quality, StockId} from "./Stock";

export interface Material {

  id: number;

  name: string;

  recyclable: boolean;

  yearUsedQuantities: [{[key: number]: number}];

  usedQuantity: number;

  stockIds: [StockId];
}
