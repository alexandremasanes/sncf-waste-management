/**
 * Created by alexandremasanes on 29/03/2018.
 */

export interface ProjectType {

  id: number;

  name: string;

  code: string;
}
