
export interface Triple<L, M, R> {

  left: L;

  middle: M;

  right: R;
}
