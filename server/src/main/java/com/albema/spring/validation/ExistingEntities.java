package com.albema.spring.validation;

import javax.validation.Constraint;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
@Constraint(validatedBy = ExistingEntitiesValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExistingEntities {

    ExistingEntity[] value();
}
