package com.albema.spring.validation;

import com.albema.common.orm.entity.BaseEntity;
import com.albema.common.orm.entity.identifiable.IdentifiedByIdEntity;

import javax.validation.Constraint;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
@Constraint(validatedBy = ExistingEntityValidator.class)
@Target({
        ElementType.FIELD,
        ElementType.PARAMETER
})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExistingEntity {

    Class<? extends IdentifiedByIdEntity> value();

    String identifierProperty() default "id";
}
