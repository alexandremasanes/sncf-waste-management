package com.albema.spring.validation;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintValidatorContext;
import java.io.Serializable;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
public class ExistingEntitiesValidator extends BusinessDataValidator<ExistingEntities> {

    //TODO finish
    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public boolean isValid(
            Serializable serializable,
            ConstraintValidatorContext constraintValidatorContext
    ) {
        String stm;

        Query<Boolean> query;

        int i;

        String openJoinPart;

        String[] whereAnd;

        if(getConstraintAnnotation().value().length == 0)
            return true;

        whereAnd = new String[getConstraintAnnotation().value().length];

        openJoinPart = "";

        for(i = 0; i < getConstraintAnnotation().value().length; ++i) {
            openJoinPart += getConstraintAnnotation().value()[i].value().getName() + " e" + i;
            whereAnd[i] = 'e' + i + "=:value" + i;
        }

        stm = "SELECT EXISTS("
                +     "FROM " + openJoinPart + " "
                +     "WHERE " + StringUtils.join(whereAnd, " AND ")
                + ")";

        query = getCurrentSession().createQuery(stm);

        for(i = 0; i < whereAnd.length; ++i)
            query.setParameter("e" + i, serializable);
        return query.uniqueResult();
    }

    ExistingEntitiesValidator(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
