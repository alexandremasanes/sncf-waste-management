package com.albema.spring.validation;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import javax.validation.ConstraintValidatorContext;
import java.io.Serializable;

/**
 * Created by alexandremasanes on 27/03/2018.
 */
@Component
class UniqueEntityPropertyValidator extends BusinessDataValidator<UniqueEntityProperty> {

    UniqueEntityPropertyValidator(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isValid(Serializable o, ConstraintValidatorContext constraintValidatorContext) {
        String stm;

        String entityProperty;

        entityProperty = resolveFieldName(o, getConstraintAnnotation()::entityProperty);

        stm = "SELECT NOT EXISTS("
            +     "FROM " + getConstraintAnnotation().entityClass() + " "
            +     "WHERE " + entityProperty + "=:value"
            + ")";

        return (boolean) getCurrentSession().createQuery(stm)
                                            .setParameter("value", o)
                                            .uniqueResult();
    }
}