package com.albema.spring.validation;

import com.albema.common.orm.entity.BaseEntity;
import com.albema.spring.validation.UniqueEntityPropertyValidator;

import javax.validation.Constraint;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by alexandremasanes on 27/03/2018.
 */
@Constraint(validatedBy = UniqueEntityPropertyValidator.class)
@Target({
        ElementType.FIELD,
        ElementType.PARAMETER
})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueEntityProperty {

    Class<? extends BaseEntity> entityClass();

    String entityProperty() default "";

    String attribute() default "";
}
