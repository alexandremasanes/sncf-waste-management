package com.albema.spring.validation;

import javax.validation.Constraint;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by alexandremasanes on 27/03/2018.
 */
@Constraint(validatedBy = UniqueConstraintsValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueConstraints {

    UniqueEntityProperty[] value();
}
