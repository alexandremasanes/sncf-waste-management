package com.albema.spring.security;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.UUID;
import java.util.function.Function;

/**
 * Created by alexandremasanes on 29/01/2018.
 */
@Component
public class TokenEncryptionManager {

    public TokenEncryptionComponents toTokenEncryptionComponents(
            UUID uuid,
            String emailAddress,
            Timestamp timestamp
    ) {
        TokenEncryptionComponents components;

        components = new TokenEncryptionComponents(uuid, emailAddress, timestamp);

        return components;
    }

    public TokenEncryptionComponents fromEncryptedString(
            String encryptedString,
            Function<String, String> decryptionFunction
    ) {
        String[]                  decryptedStrings;
        TokenEncryptionComponents tokenEncryptionComponents;

        decryptedStrings = decryptionFunction.apply(encryptedString).split("#");
        try {
            tokenEncryptionComponents = new TokenEncryptionComponents(
                    UUID.fromString(decryptedStrings[0]), decryptedStrings[1], Timestamp.valueOf(decryptedStrings[2]));
        } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException e) {
            tokenEncryptionComponents = null;
        }

        return tokenEncryptionComponents;
    }

    public String toEncryptedString(TokenEncryptionComponents tokenEncryptionComponents, Function<String, String> encryptionFunction) {
        String key;

        key = tokenEncryptionComponents.getUuid().toString() + '#' +
              tokenEncryptionComponents.getEmailAddress() + '#' +
              tokenEncryptionComponents.getTimestamp() + '#';

        return encryptionFunction.apply(key);
    }

    public static class TokenEncryptionComponents {
        UUID uuid;
        String    emailAddress;
        Timestamp timestamp;

        public TokenEncryptionComponents(UUID uuid, String emailAddress, Timestamp timestamp) {
            this.uuid = uuid;
            this.emailAddress = emailAddress;
            this.timestamp = timestamp;
        }

        public UUID getUuid() {
            return uuid;
        }

        public String getEmailAddress() {
            return emailAddress;
        }

        public Timestamp getTimestamp() {
            return timestamp;
        }

        @Override
        public boolean equals(Object o) {
            return o == this ||
                   o instanceof TokenEncryptionComponents &&
                   uuid.equals(((TokenEncryptionComponents) o).uuid) &&
                   emailAddress.equals(((TokenEncryptionComponents) o)
                               .emailAddress) &&
                   timestamp.equals(((TokenEncryptionComponents) o)
                            .timestamp);
        }
    }
}