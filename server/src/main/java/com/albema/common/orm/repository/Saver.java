package com.albema.common.orm.repository;

import com.albema.common.orm.entity.RecordableEntity;

/**
 * Created by alexandremasanes on 21/02/2017.
 */
public interface Saver<T extends RecordableEntity> {

    void save(T entity);
}