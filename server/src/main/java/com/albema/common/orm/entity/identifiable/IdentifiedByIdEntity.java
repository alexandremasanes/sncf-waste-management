package com.albema.common.orm.entity.identifiable;

import com.albema.common.orm.entity.RecordableEntity;
import com.albema.common.util.ArrayUtils;

import org.apache.commons.lang3.builder.EqualsBuilder;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
public abstract class IdentifiedByIdEntity extends RecordableEntity implements IdentifiableById {

	private static final long serialVersionUID = -5075953598452663724L;

	public static final String ID_GENERATOR_NAME = "idGenerator";

	@Id
	@Access(AccessType.PROPERTY) //allows calling getId without loading whole proxy (use final instead ?)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ID_GENERATOR_NAME)
	private long id;

	private transient Serializable[] businessKey;

	@Override
	public long getId() {
		return id;
	}

	void setId(long id) {
		this.id = id;
	}
}