package com.albema.common.util;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by alexandremasanes on 01/03/2017.
 */
public final class ObjectUtils {

    public static <T> T ifNull(
            T value,
            T other
    ) {
        return value != null ? value : other;
    }

    public static <T> T ifNull(
            T           value,
            Supplier<T> supplier
    ) {
        return value != null ? value : supplier.get();
    }

    public static <T> T ifNull(
            T                   value,
            Function<Object, T> function,
            Object              functionParameter
    ) {
        return value != null ? value : function.apply(functionParameter);
    }

    public static <O, T> T ifNotNull(O instance, Function<? super O, ? extends T> method, T nullSubstition) {
        return instance == null ? nullSubstition : method.apply(instance);
    }

    public static <O, T> T ifNotNull(Object instance, Object[] params,  Function<Object[], ? extends T> method, T nullSubstition) {
        params = ArrayUtils.unshift(params, instance);
        return instance == null ? nullSubstition : method.apply(params);
    }

    public static <O> void ifNotNull(O instance, Consumer<? super O> method) {
        if(instance != null)
            method.accept(instance);
    }

    public static void ifNotNull(Object instance, Consumer<Object> consumer, Object value) {
        if(instance != null)
            consumer.accept(new Object[] {instance, value});
    }

    public static <T, U> void ifNotNull(T instance, BiConsumer<T, U> consumer, U value) {
        if(instance != null)
            consumer.accept(instance, value);
    }

    public static void ifNotNull(Object instance, Consumer<Object[]> method, Object[] params) {
        if(instance != null) {
            params = ArrayUtils.unshift(params, instance);
            method.accept(params);
        }
    }

    public static void doIf(Runnable runnable, boolean bool) {
        if(bool)
            runnable.run();
    }

    ObjectUtils() {}
}