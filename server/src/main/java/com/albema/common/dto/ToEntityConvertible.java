package com.albema.common.dto;

import com.albema.common.orm.entity.identifiable.IdentifiedByIdEntity;

/**
 * Created by alexandremasanes on 26/08/2017.
 */
public interface ToEntityConvertible<T extends IdentifiedByIdEntity> {

    T toEntity();
}
