package com.alda.sncf.wastemanagement.business.mapping.entity.user;

import com.alda.sncf.wastemanagement.business.mapping.entity.Company;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by alexandremasanes on 30/01/2018.
 */
@Entity
@Table(name = "third_parties")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ThirdParty extends User {

    @Getter
    @ManyToOne
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    private Company company;

    public ThirdParty(String emailAddress, Company company) {
        super(emailAddress);
        this.company = company;
        company.addThirdParty(this);
    }
}