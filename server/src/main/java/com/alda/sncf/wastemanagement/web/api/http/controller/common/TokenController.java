package com.alda.sncf.wastemanagement.web.api.http.controller.common;

import com.albema.common.http.error.CustomHttpExceptions;
import com.alda.sncf.wastemanagement.business.mapping.entity.Token;
import com.alda.sncf.wastemanagement.web.api.model.input.TokenCreation;
import com.alda.sncf.wastemanagement.web.api.model.output.TokenDTO;
import com.alda.sncf.wastemanagement.web.common.business.helper.TokenManager;
import com.alda.sncf.wastemanagement.business.logic.UserService;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;

/**
 * Created by alexandremasanes on 01/02/2018.
 */
@RestController
@RequestMapping("/p/tokens")
public class TokenController {

    private final UserService userService;

    private final TokenManager tokenManager;

    public TokenController(UserService userService, TokenManager tokenManager) {
        this.userService = userService;
        this.tokenManager = tokenManager;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TokenDTO onPost(
            @RequestBody @Valid TokenCreation tokenCreation
    ) throws NoSuchAlgorithmException {
        Token token;

        token = userService.createToken(tokenCreation);

        if(token == null)
            throw new CustomHttpExceptions.BadRequestException();

        return new TokenDTO(
                token.getUser(),
                tokenManager.createEncrypted(token.getUser())
        );
    }
}
