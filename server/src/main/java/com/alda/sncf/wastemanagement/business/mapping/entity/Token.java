package com.alda.sncf.wastemanagement.business.mapping.entity;

import com.albema.common.orm.entity.RecordableEntity;
import com.alda.sncf.wastemanagement.business.mapping.entity.user.User;

import lombok.*;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

/**
 * Created by alexandremasanes on 01/02/2018.
 */
@Entity
@Table(name = "tokens")
@EqualsAndHashCode(of = "user")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Token extends RecordableEntity {

    @EqualsAndHashCode(of = "user")
    protected static class Identifier implements Serializable {

        private User user;
    }

    @Getter
    @Id
    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "id", referencedColumnName = "id")
    private User user;

    @Getter
    private UUID value;

    @Getter
    @Generated(GenerationTime.INSERT)
    @Column(name = "creation_time")
    private Timestamp creationTime;

    public Token(User user) {
        this.user = user;
        user.setToken(this);
        value = UUID.randomUUID();
    }
}