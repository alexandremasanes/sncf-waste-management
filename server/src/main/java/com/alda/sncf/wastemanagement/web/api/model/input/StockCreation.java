package com.alda.sncf.wastemanagement.web.api.model.input;

import static com.alda.sncf.wastemanagement.business.mapping.entity.Stock.Quality;

import com.albema.spring.validation.ExistingEntity;
import com.alda.sncf.wastemanagement.business.logic.MaterialService;
import com.alda.sncf.wastemanagement.business.mapping.entity.Material;
import com.alda.sncf.wastemanagement.business.mapping.entity.Stock;

import lombok.Getter;
import org.checkerframework.checker.nullness.qual.NonNull;

import javax.validation.constraints.Min;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
public class StockCreation extends StockUpdate implements MaterialService.StockCreation, Serializable {

    @Getter
    @ExistingEntity(Material.class)
    private long materialId;

    @Getter
    @NonNull
    private Quality quality;
}