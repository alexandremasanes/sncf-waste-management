package com.alda.sncf.wastemanagement.web.common.repository;

import com.albema.spring.repository.DAO;
import com.alda.sncf.wastemanagement.business.mapping.entity.Token;
import com.alda.sncf.wastemanagement.business.repository.TokenDAO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Created by alexandremasanes on 01/02/2018.
 */
@Repository
class TokenDAOImpl extends DAO implements TokenDAO {

    @Override
    public Token find(UUID value) {
        final String stm;

        stm = "FROM " + Token.class.getName() + " "
            + "WHERE value=:value";

        return (Token) getCurrentSession().createQuery(stm)
                                       .setParameter("value", value)
                                       .uniqueResult();
    }

    @Override
    public boolean removeExpiredTokens(int tokenLifeTime) {
        final String stm;

        stm = "DELETE FROM " + Token.class.getName() + " "
            + "WHERE SECOND(CURRENT_TIMESTAMP()) - SECOND(creationTime) > :tokenLifeTime";

        return getCurrentSession().createQuery(stm)
                               .setParameter("tokenLifeTime", tokenLifeTime)
                               .executeUpdate() != 0;
    }

    @Override
    public boolean remove(Token token) {
        token.getUser().setToken(null);
        return remove0(token);
    }

    @Override
    public void save(Token entity) {
        save0(entity);
    }

    protected TokenDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
