package com.alda.sncf.wastemanagement.business.logic;

import com.alda.sncf.wastemanagement.business.mapping.entity.Material;
import com.alda.sncf.wastemanagement.business.mapping.entity.Stock;
import com.alda.sncf.wastemanagement.business.repository.StockDAO;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by alexandremasanes on 02/02/2018.
 */
public interface MaterialService {

    interface MaterialCreation {

        String getCode();

        String getName();

        boolean isRecyclable();
    }

    interface StockCreation {

        long getMaterialId();

        Stock.Quality getQuality();

        BigDecimal getQuantity();
    }

    interface StockUpdate {

        BigDecimal getQuantity();
    }

    List<Material> get(long... ids);

    List<Material> getAll();

    Material getOne(long id);

    Material createStock(MaterialCreation materialCreation);

    List<Stock> getAllStocks();

    List<Stock> getMaterialStocks(long materialId);

    List<Stock> getQualityStocks(Stock.Quality quality);

    Stock getOneStock(long materialId, Stock.Quality quality);

    Stock createStock(StockCreation stockCreation);

    void updateStock(long materialId, Stock.Quality quality, StockUpdate stockUpdate);

    void deleteStocks(StockDAO.MaterialIdAndQuality... materialIdAndQualities);

    void deleteStock(long materialId, Stock.Quality quality);
}