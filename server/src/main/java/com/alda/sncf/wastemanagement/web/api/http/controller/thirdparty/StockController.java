package com.alda.sncf.wastemanagement.web.api.http.controller.thirdparty;

import com.alda.sncf.wastemanagement.business.logic.MaterialService;
import com.alda.sncf.wastemanagement.web.common.business.helper.TokenManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
@RestController("thirdPartyStockController")
@RequestMapping("/tp/stocks")
public class StockController extends Controller {

    private final MaterialService materialService;

    public StockController(TokenManager tokenManager, MaterialService materialService) {
        super(tokenManager);
        this.materialService = materialService;
    }
}
