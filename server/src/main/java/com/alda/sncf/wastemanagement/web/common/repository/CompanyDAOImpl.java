package com.alda.sncf.wastemanagement.web.common.repository;

import com.albema.spring.repository.DAO;
import com.alda.sncf.wastemanagement.business.mapping.entity.Company;
import com.alda.sncf.wastemanagement.business.repository.CompanyDAO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
class CompanyDAOImpl extends DAO implements CompanyDAO {

    @Override
    public List<Company> find() {
        return find(Company.class);
    }

    @Override
    public List<Company> find(long... ids) {
        return find(Company.class, ids);
    }

    @Override
    public Company find(long id) {
        return find(Company.class, id);
    }

    @Override
    public boolean has(long id) {
        return false;
    }

    @Override
    public long getNextId() {
        return 0;
    }

    @Override
    public boolean remove(long... ids) {
        return remove(Company.class, ids);
    }

    @Override
    public boolean remove(long id) {
        return remove(Company.class, id);
    }

    @Override
    public boolean remove(Company company) {
        return remove0(company);
    }

    @Override
    public boolean remove(Company... company) {
        return remove0(company);
    }

    @Override
    public void save(Company entity) {
        save0(entity);
    }

    protected CompanyDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}