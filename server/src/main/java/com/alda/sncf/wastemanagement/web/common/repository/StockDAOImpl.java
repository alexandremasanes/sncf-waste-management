package com.alda.sncf.wastemanagement.web.common.repository;

import com.albema.spring.repository.DAO;
import com.alda.sncf.wastemanagement.business.mapping.entity.Stock;
import com.alda.sncf.wastemanagement.business.repository.StockDAO;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.function.Function;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
@Repository
class StockDAOImpl extends DAO implements StockDAO {

    @Override
    public void save(Stock entity) {
        save0(entity);
    }

    @Override
    public List<Stock> find() {
        return find(Stock.class);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Stock> find(long materialId) {

        String stm;

        stm = "FROM " + Stock.class.getName() + " "
            + "WHERE material.id=:materialId";

        return getCurrentSession().createQuery(stm)
                               .setParameter("materialId", materialId)
                               .list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Stock> find(Stock.Quality quality) {
        String stm;

        stm = "FROM " + Stock.class.getName() + " s "
            + "WHERE quality=:quality";

        return getCurrentSession().createQuery(stm)
                                  .setParameter("quality", quality)
                                  .list();
    }

    @Override
    public Stock find(long materialId, Stock.Quality quality) {
        String stm;

        stm = "FROM " + Stock.class.getName() + " "
            + "WHERE material.id=:materialId AND quality=:quality";

        return (Stock) getCurrentSession().createQuery(stm)
                                          .setParameter("materialId", materialId)
                                          .setParameter("quality", quality)
                                          .uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Stock> find(MaterialIdAndQuality... materialIdAndQualities) {
        return find(
                Stock.class,
                materialIdAndQualities,
                new String[] {
                        "material.id",
                        "quality"
                },
                new Function[] {
                        (Function<MaterialIdAndQuality, Serializable>) MaterialIdAndQuality::getMaterialId,
                        (Function<MaterialIdAndQuality, Serializable>) MaterialIdAndQuality::getQuality
                }
        );
    }

    @Override
    public boolean remove(long materialId, Stock.Quality quality) {

        final String stm;

        stm = "DELETE FROM " + Stock.class + " "
            + "WHERE material.materialId=:materialId AND quality=:quality";

        return getCurrentSession().createQuery(stm)
                                  .executeUpdate() != 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean remove(MaterialIdAndQuality... materialIdAndQualities) {
        return remove(
                Stock.class,
                materialIdAndQualities,
                new String[] {
                        "material.id",
                        "quality"
                },
                new Function[] {
                        (Function<MaterialIdAndQuality, Serializable>) MaterialIdAndQuality::getMaterialId,
                        (Function<MaterialIdAndQuality, Serializable>) MaterialIdAndQuality::getQuality
                }
        );
    }

    protected StockDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}