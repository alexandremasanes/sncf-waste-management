package com.alda.sncf.wastemanagement.web.api.model.common;

import com.alda.sncf.wastemanagement.business.logic.ProjectService;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.math.BigDecimal;

public class StockIdQuantity implements ProjectService.StockIdQuantity, Serializable {

    @Valid
    @Getter
    private StockId stockId;

    @Min(0)
    @Getter
    private BigDecimal quantity;

    public StockIdQuantity(StockId stockId, BigDecimal quantity) {
        this.stockId = stockId;
        this.quantity = quantity;
    }
}