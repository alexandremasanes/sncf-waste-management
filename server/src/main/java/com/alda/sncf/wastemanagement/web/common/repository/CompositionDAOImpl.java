package com.alda.sncf.wastemanagement.web.common.repository;

import com.albema.spring.repository.DAO;
import com.alda.sncf.wastemanagement.business.mapping.entity.Composition;
import com.alda.sncf.wastemanagement.business.repository.CompositionDAO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
class CompositionDAOImpl extends DAO implements CompositionDAO {

    //@Override
    public void save(Composition entity) {
        //save0(entity);
    }

    protected CompositionDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}