package com.alda.sncf.wastemanagement.web.common.repository;

import com.albema.spring.repository.DAO;
import com.alda.sncf.wastemanagement.business.mapping.entity.Company;
import com.alda.sncf.wastemanagement.business.mapping.entity.Material;
import com.alda.sncf.wastemanagement.business.mapping.entity.Project;
import com.alda.sncf.wastemanagement.business.repository.ProjectDAO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
class ProjectDAOImpl extends DAO implements ProjectDAO {

    static class QuantityByMaterialAndCompanyImpl implements QuantityByMaterialAndCompany {

        private Material material;

        private Company company;

        private BigDecimal quantity;

        public QuantityByMaterialAndCompanyImpl(Material material, Company company, BigDecimal quantity) {
            this.material = material;
            this.company = company;
            this.quantity = quantity;
        }

        @Override
        public Material getMaterial() {
            return material;
        }

        @Override
        public Company getCompany() {
            return company;
        }

        @Override
        public BigDecimal getQuantity() {
            return quantity;
        }
    }

    @Override
    public List<Project> find() {
        return find(Project.class);
    }

    @Override
    public List<Project> find(long... ids) {
        return find(Project.class, ids);
    }

    @Override
    public Project find(long id) {
        return find(Project.class, id);
    }

    @Override
    public boolean has(long id) {
        return false;
    }

    @Override
    public long getNextId() {
        return 0;
    }

    @Override
    public boolean remove(long... ids) {
        return remove(Project.class, ids);
    }

    @Override
    public boolean remove(long id) {
        return remove(Material.class, id);
    }

    @Override
    public boolean remove(Project company) {
        return remove0(company);
    }

    @Override
    public boolean remove(Project... company) {
        return remove0(company);
    }

    @Override
    public void save(Project entity) {
        save0(entity);
    }

    @Override @SuppressWarnings("unchecked")
    public List<QuantityByMaterialAndCompany> findQuantitiesByMaterialAndCompany() {
        String stm;

        stm = "SELECT NEW " + QuantityByMaterialAndCompanyImpl.class.getName() + "("
            +   "material, company, SUM(composition.quantity)"
            + ") "
            + "FROM " + Project.class.getName() + " project "
            +   "RIGHT JOIN project.compositions AS composition "
            +   "RIGHT JOIN composition.stock.material AS material "
            +   "INNER JOIN project.company AS company "
            + "GROUP BY material, company";

        return getCurrentSession().createQuery(stm)
                                  .list();
    }

    ProjectDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}