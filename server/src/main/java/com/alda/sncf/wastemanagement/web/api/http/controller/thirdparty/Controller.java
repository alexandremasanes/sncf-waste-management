package com.alda.sncf.wastemanagement.web.api.http.controller.thirdparty;

import com.albema.common.http.error.CustomHttpExceptions;
import com.alda.sncf.wastemanagement.business.mapping.entity.Token;
import com.alda.sncf.wastemanagement.business.mapping.entity.user.ThirdParty;
import com.alda.sncf.wastemanagement.web.common.business.helper.TokenManager;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;

public abstract class Controller {

    private final TokenManager tokenManager;

    @ModelAttribute
    public void beforeHandle(
            @RequestHeader("Authorization") String encryptedToken
    ) {
        Token token;

        token = tokenManager.get(encryptedToken);

        if(token == null)
            throw new CustomHttpExceptions.UnauthorizedRequestException();

        if (!(token.getUser() instanceof ThirdParty))
            throw new CustomHttpExceptions.ResourceForbiddenException();
    }

    protected Controller(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }
}