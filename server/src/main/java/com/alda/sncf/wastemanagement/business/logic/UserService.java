package com.alda.sncf.wastemanagement.business.logic;

import com.alda.sncf.wastemanagement.business.mapping.entity.Token;
import com.alda.sncf.wastemanagement.business.mapping.entity.user.User;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

/**
 * Created by alexandremasanes on 02/02/2018.
 */
public interface UserService {

    interface UserCreation {

    }

    interface TokenCreation {

        String getEmailAddress();

        String getPassword();
    }

    User getOne(long id);

    User getOne(String emailAddress);

    List<User> getAll();

    List<User> get(long... ids);

    Token getOneToken(UUID value);

    Token createToken(TokenCreation tokenCreation) throws NoSuchAlgorithmException;

    void update(User user);

    void removeExpiredTokens();
}