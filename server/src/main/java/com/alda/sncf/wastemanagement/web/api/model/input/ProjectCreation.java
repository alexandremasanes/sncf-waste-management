package com.alda.sncf.wastemanagement.web.api.model.input;


import com.albema.spring.validation.ExistingEntity;
import com.alda.sncf.wastemanagement.business.logic.ProjectService;
import com.alda.sncf.wastemanagement.business.mapping.entity.Project;
import com.alda.sncf.wastemanagement.business.mapping.entity.ProjectType;
import com.alda.sncf.wastemanagement.business.mapping.entity.Stock;
import lombok.Getter;

import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by alexandremasanes on 01/02/2018.
 */

public class ProjectCreation implements ProjectService.ProjectCreation, Serializable {

    @Getter
    @ExistingEntity(ProjectType.class)
    private long projectTypeId;

    @Getter
    @ExistingEntity(Project.class)
    private long thirdPartyId;

    @Getter
    @Positive
    private long materialId;

    @Getter
    @NotNull
    private Stock.Quality quality;

    @Getter
    @Positive
    private BigDecimal quantity;

    @Getter
    @NotEmpty @NotBlank
    private String description;

    @Getter
    @Future
    private Date dueDate;
}