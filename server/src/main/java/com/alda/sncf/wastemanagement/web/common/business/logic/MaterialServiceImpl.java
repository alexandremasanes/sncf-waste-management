package com.alda.sncf.wastemanagement.web.common.business.logic;

import com.albema.common.util.ObjectUtils;
import com.alda.sncf.wastemanagement.business.logic.MaterialService;
import com.alda.sncf.wastemanagement.business.mapping.entity.Material;
import com.alda.sncf.wastemanagement.business.mapping.entity.Stock;
import com.alda.sncf.wastemanagement.business.repository.MaterialDAO;
import com.alda.sncf.wastemanagement.business.repository.StockDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.BiConsumer;

@Service
class MaterialServiceImpl implements MaterialService {

    private final MaterialDAO materialDAO;

    private final StockDAO    stockDAO;

    @Override
    @Transactional(readOnly = true)
    public List<Material> getAll() {
        return materialDAO.find();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Material> get(long... ids) {
        return materialDAO.find(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public Material getOne(long id) {
        return materialDAO.find(id);
    }

    @Override
    @Transactional
    public Material createStock(MaterialCreation materialCreation) {

        final Material material;

        material = new Material(materialCreation.getCode());

        material.setName(materialCreation.getName());

        material.setRecyclable(materialCreation.isRecyclable());

        materialDAO.save(material);

        return material;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Stock> getAllStocks() {
        return stockDAO.find();
    }

    @Transactional(readOnly = true)
    public List<Stock> getStocks(StockDAO.MaterialIdAndQuality... materialIdAndQualities) {
        return stockDAO.find(materialIdAndQualities);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Stock> getMaterialStocks(long materialId) {
        return stockDAO.find(materialId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Stock> getQualityStocks(Stock.Quality quality) {
        return stockDAO.find(quality);
    }

    @Override
    @Transactional(readOnly = true)
    public Stock getOneStock(long materialId, Stock.Quality quality) {
        return stockDAO.find(materialId, quality);
    }

    @Override
    @Transactional
    public Stock createStock(StockCreation stockCreation) {
        final Material material;
        final Stock stock;

        material = materialDAO.find(stockCreation.getMaterialId());

        stock = new Stock(material, stockCreation.getQuality());

        stock.setQuantity(stockCreation.getQuantity());

        stockDAO.save(stock);

        return stock;
    }

    @Override
    @Transactional
    public void updateStock(long materialId, Stock.Quality quality, StockUpdate stockUpdate) {
        final Stock stock;

        stock = stockDAO.find(materialId, quality);

        ObjectUtils.ifNotNull(
                stock,
                ((BiConsumer<Stock, BigDecimal>) Stock::setQuantity).andThen(
                        (s, t) -> stockDAO.save(s)
                ),
                stockUpdate.getQuantity()
        );
    }

    @Override
    @Transactional
    public void deleteStocks(StockDAO.MaterialIdAndQuality... materialIdAndQualities) {
        stockDAO.remove(materialIdAndQualities);
    }

    @Override
    @Transactional
    public void deleteStock(long materialId, Stock.Quality quality) {
        stockDAO.remove(materialId, quality);
    }

    MaterialServiceImpl(MaterialDAO materialDAO, StockDAO stockDAO) {
        this.materialDAO = materialDAO;
        this.stockDAO = stockDAO;
    }
}