package com.alda.sncf.wastemanagement.business.repository;

import com.albema.common.orm.repository.Finder;
import com.albema.common.orm.repository.Remover;
import com.albema.common.orm.repository.Saver;
import com.alda.sncf.wastemanagement.business.mapping.entity.Company;

public interface CompanyDAO extends Finder<Company>, Saver<Company>, Remover<Company> {
}
