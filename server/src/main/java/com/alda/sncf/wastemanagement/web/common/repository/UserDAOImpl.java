package com.alda.sncf.wastemanagement.web.common.repository;

import com.albema.spring.repository.DAO;
import com.alda.sncf.wastemanagement.business.mapping.entity.Material;
import com.alda.sncf.wastemanagement.business.mapping.entity.user.User;
import com.alda.sncf.wastemanagement.business.repository.UserDAO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
class UserDAOImpl extends DAO implements UserDAO {

    @Override
    public List<User> find() {
        return find(User.class);
    }

    @Override
    public List<User> find(long... ids) {
        return find(User.class, ids);
    }

    @Override
    public User find(long id) {
        return find(User.class, id);
    }

    @Override
    public boolean has(long id) {
        return false;
    }

    @Override
    public long getNextId() {
        return 0;
    }

    @Override
    public boolean remove(long... ids) {
        return remove(User.class, ids);
    }

    @Override
    public boolean remove(long id) {
        return remove(Material.class, id);
    }

    @Override
    public boolean remove(User company) {
        return remove0(company);
    }

    @Override
    public boolean remove(User... company) {
        return remove0(company);
    }

    @Override
    public void save(User entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public User findByEmailAddressAndHash(String emailAddress, String hash) {
        final String stm;

        stm = "FROM " + User.class.getName() + " "
            + "WHERE emailAddress=:emailAddress AND hash=:hash";

        return (User) getCurrentSession().createQuery(stm)
                                         .setParameter("emailAddress", emailAddress)
                                         .setParameter("hash", hash)
                                         .uniqueResult();
    }

    @Override
    public User findByEmailAddress(String emailAddress) {
        final String stm;

        stm = "FROM " + User.class.getName() + " u "
            + "WHERE u.emailAddress=:emailAddress";

        return (User) getCurrentSession().createQuery(stm)
                                         .setParameter("emailAddress", emailAddress)
                                         .uniqueResult();
    }

    protected UserDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}