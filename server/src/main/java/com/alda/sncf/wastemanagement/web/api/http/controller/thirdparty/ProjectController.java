package com.alda.sncf.wastemanagement.web.api.http.controller.thirdparty;

import com.alda.sncf.wastemanagement.business.logic.ProjectService;
import com.alda.sncf.wastemanagement.business.mapping.entity.Project;
import com.alda.sncf.wastemanagement.web.api.model.input.ProjectCreation;
import com.alda.sncf.wastemanagement.web.api.model.output.DTO;
import com.alda.sncf.wastemanagement.web.api.model.output.ProjectDTO;

import com.alda.sncf.wastemanagement.web.common.business.helper.TokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController("thirdPartyProjectController")
@RequestMapping("/tp/projects")
public class ProjectController extends Controller {

    private ProjectService projectService;

    public ProjectController(TokenManager tokenManager, ProjectService projectService) {
        super(tokenManager);
        this.projectService = projectService;
    }

    @GetMapping
    public List<ProjectDTO> onGet() {
        return DTO.fromCollection(projectService.getAll(), ProjectDTO::new);
    }

    @GetMapping(params = "ids")
    public List<ProjectDTO> onGet(@RequestParam long[] ids) {
        return DTO.fromCollection(projectService.get(ids), ProjectDTO::new);
    }

    @GetMapping("/{id}")
    public ProjectDTO onGet(@PathVariable long id) {
        Project project;

        project = projectService.getOne(id);

        return project != null ? new ProjectDTO(project) : null;
    }

    @PostMapping
    public ProjectDTO onPost(@RequestBody @Valid ProjectCreation projectCreation) {
        return new ProjectDTO(projectService.create(projectCreation));
    }
}
