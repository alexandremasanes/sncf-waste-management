package com.alda.sncf.wastemanagement.web.api.http.controller.manager;

import com.alda.sncf.wastemanagement.business.logic.MaterialService;
import com.alda.sncf.wastemanagement.business.mapping.entity.Stock;
import com.alda.sncf.wastemanagement.web.api.model.input.StockCreation;
import com.alda.sncf.wastemanagement.web.api.model.input.StockUpdate;
import com.alda.sncf.wastemanagement.web.api.model.output.StockDTO;

import com.alda.sncf.wastemanagement.web.common.business.helper.TokenManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
@RestController("managerStockController")
@RequestMapping("/m/stocks")
public class StockController extends Controller {

    private final MaterialService materialService;

    public StockController(TokenManager tokenManager, MaterialService materialService) {
        super(tokenManager);
        this.materialService = materialService;
    }

    @GetMapping
    public List<StockDTO> onGet() {
        List<Stock> stocks;

        stocks = materialService.getAllStocks();

        return StockDTO.fromCollection(stocks);
    }

    @GetMapping(params = "materialId")
    public List<StockDTO> onGet(
            @RequestParam long materialId
    ) {
        List<Stock> stocks;

        stocks = materialService.getMaterialStocks(materialId);

        return StockDTO.fromCollection(stocks);
    }

    @GetMapping(params = "quality")
    public List<StockDTO> onGet(
            @RequestParam Stock.Quality quality
    ) {
        List<Stock> stocks;

        stocks = materialService.getQualityStocks(quality);

        return StockDTO.fromCollection(stocks);
    }

    @GetMapping(params = {"materialId", "quality"})
    public StockDTO onGet(
            @RequestParam long materialId,
            @RequestParam Stock.Quality quality
    ) {
        Stock stock;

        stock = materialService.getOneStock(materialId, quality);

        return stock != null ? new StockDTO(stock) : null;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public StockDTO onPost(
            @RequestBody @Valid StockCreation stockCreation
    ) {
        Stock stock;

        stock = materialService.createStock(stockCreation);

        return stock != null ? new StockDTO(stock) : null;
    }

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void onPut(
            @RequestParam long          materialId,
            @RequestParam Stock.Quality quality,
            @RequestBody  StockUpdate   stockUpdate
    ) {
        materialService.updateStock(materialId, quality, stockUpdate);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void onDelete(
            @RequestParam long          materialId,
            @RequestParam Stock.Quality quality
    ) {
        materialService.deleteStock(materialId, quality);
    }
}