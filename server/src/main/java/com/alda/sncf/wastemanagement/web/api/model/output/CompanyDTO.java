package com.alda.sncf.wastemanagement.web.api.model.output;

import com.alda.sncf.wastemanagement.business.mapping.entity.Company;

/**
 * Created by alexandremasanes on 31/01/2018.
 */
@SuppressWarnings("all")
public class CompanyDTO extends DTO<Company> {

    private String sirenNumber;

    private String name;

    private long[] projectIds;

    private long[] thirdPartyIds;

    public CompanyDTO(Company entity) {
        super(entity);
        sirenNumber = entity.getSirenNumber();
        name = entity.getName();
        projectIds = getIds(entity.getProjects());
        thirdPartyIds = getIds(entity.getThirdParties());
    }
}
