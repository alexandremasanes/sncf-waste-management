package com.alda.sncf.wastemanagement.web.api.model.output;

import com.alda.sncf.wastemanagement.business.mapping.entity.user.Manager;
import com.alda.sncf.wastemanagement.business.mapping.entity.user.ThirdParty;
import com.alda.sncf.wastemanagement.business.mapping.entity.user.User;
import com.alda.sncf.wastemanagement.web.api.model.output.user.ManagerDTO;
import com.alda.sncf.wastemanagement.web.api.model.output.user.ThirdPartyDTO;
import com.alda.sncf.wastemanagement.web.api.model.output.user.UserDTO;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by alexandremasanes on 01/02/2018.
 */
@SuppressWarnings("all")
public class TokenDTO implements Serializable {

    @SerializedName("user")
    private UserDTO userDTO;

    private String value;

    public TokenDTO(User user, String value) {
        userDTO = user instanceof Manager ?
                new ManagerDTO((Manager) user) :
                new ThirdPartyDTO((ThirdParty) user);
        this.value = value;
    }
}
