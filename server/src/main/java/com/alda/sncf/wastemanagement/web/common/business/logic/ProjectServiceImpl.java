package com.alda.sncf.wastemanagement.web.common.business.logic;

import com.alda.sncf.wastemanagement.business.logic.ProjectService;
import com.alda.sncf.wastemanagement.business.mapping.entity.*;
import com.alda.sncf.wastemanagement.business.mapping.entity.user.ThirdParty;
import com.alda.sncf.wastemanagement.business.repository.ProjectDAO;
import com.alda.sncf.wastemanagement.business.repository.ProjectTypeDAO;
import com.alda.sncf.wastemanagement.business.repository.StockDAO;
import com.alda.sncf.wastemanagement.business.repository.UserDAO;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
class ProjectServiceImpl implements ProjectService {

    private final ProjectDAO     projectDAO;

    private final ProjectTypeDAO projectTypeDAO;

    private final UserDAO        userDAO;

    private final StockDAO       stockDAO;

    @Override
    @Transactional(readOnly = true)
    public List<Project> getAll() {
        return projectDAO.find();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Project> get(long... ids) {
        return projectDAO.find(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public Project getOne(long id) {
        return projectDAO.find(id);
    }

    @Override
    @Transactional
    public Project create(ProjectCreation projectCreation) {

        final Project     project;
        final Date        date;
        final ProjectType projectType;
        final Company     company;
        final Stock       stock;

        date        = projectCreation.getDueDate();
        projectType = projectTypeDAO.find(projectCreation.getProjectTypeId());
        company     = ((ThirdParty) userDAO.find(projectCreation.getThirdPartyId())).getCompany();
        stock       = stockDAO.find(projectCreation.getMaterialId(), projectCreation.getQuality());


        project = new Project(date, projectType, company);

        project.setDescription(projectCreation.getDescription());

        Composition composition = new Composition(stock, project);

        composition.setQuantity(projectCreation.getQuantity());

        projectDAO.save(project);

        return project;
    }

    ProjectServiceImpl(
            ProjectDAO     projectDAO,
            ProjectTypeDAO projectTypeDAO,
            UserDAO        userDAO,
            StockDAO       stockDAO
    ) {
        this.projectDAO     = projectDAO;
        this.projectTypeDAO = projectTypeDAO;
        this.userDAO        = userDAO;
        this.stockDAO       = stockDAO;
    }
}