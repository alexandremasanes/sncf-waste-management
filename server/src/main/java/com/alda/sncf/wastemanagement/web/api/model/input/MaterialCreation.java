package com.alda.sncf.wastemanagement.web.api.model.input;

import com.albema.spring.validation.UniqueEntityProperty;
import com.alda.sncf.wastemanagement.business.logic.MaterialService;
import com.alda.sncf.wastemanagement.business.mapping.entity.Material;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


public class MaterialCreation implements MaterialService.MaterialCreation, Serializable {

    @Getter
    @NotEmpty
    private String name;

    @Getter
    @UniqueEntityProperty(entityClass = Material.class)
    private String code;

    @Getter
    private boolean recyclable;
}