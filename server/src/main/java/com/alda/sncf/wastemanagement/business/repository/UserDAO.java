package com.alda.sncf.wastemanagement.business.repository;

import com.albema.common.orm.repository.Finder;
import com.albema.common.orm.repository.Remover;
import com.albema.common.orm.repository.Saver;
import com.alda.sncf.wastemanagement.business.mapping.entity.user.User;

public interface UserDAO extends Finder<User>, Saver<User>, Remover<User>{

    User findByEmailAddressAndHash(String emailAddress, String hash);

    User findByEmailAddress(String emailAddress);
}
