package com.alda.sncf.wastemanagement.business.repository;

import com.albema.common.orm.repository.Saver;
import com.alda.sncf.wastemanagement.business.mapping.entity.Token;

import java.util.UUID;

/**
 * Created by alexandremasanes on 01/02/2018.
 */
public interface TokenDAO extends Saver<Token> {

    Token find(UUID value);

    boolean removeExpiredTokens(int tokenLifeTime);

    boolean remove(Token token);
}
