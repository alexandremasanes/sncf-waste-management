package com.alda.sncf.wastemanagement.business.mapping.entity;

import com.albema.common.orm.entity.RecordableEntity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "stocks_projects")
@EqualsAndHashCode(of = {"stock", "project"})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Composition extends RecordableEntity {

    @EqualsAndHashCode(of = {"material", "quality", "project"})
    protected static class Identifier implements Serializable {
        //jpa -> entity id type / hibernate -> entity type
        private Material material;

        private Stock.Quality quality;

        private Project project;
    }

    @Getter
    @Id
    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumns({
            @JoinColumn(
                    name                 = "material_id",
                    referencedColumnName = "id",
                    nullable             = false
            ),
            @JoinColumn(
                    name                 = "quality",
                    referencedColumnName = "quality",
                    nullable             = false
            )
    })
    private Stock stock;

    @Getter
    @Id
    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(
            name                 = "project_id",
            referencedColumnName = "id",
            nullable = false
    )
    private Project project;

    @Getter @Setter
    private BigDecimal quantity;

    public Composition(Stock stock, Project project) {
        this.stock = stock;
        this.project = project;

        stock.addComposition(this);
        project.addComposition(this);
    }
}