package com.alda.sncf.wastemanagement.business.mapping.entity;

import com.albema.common.orm.entity.identifiable.IdentifiedByIdEntity;

import lombok.*;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by alexandremasanes on 30/01/2018.
 */
@SuppressWarnings("unused")
@Entity
@Table(name = "materials")
@SequenceGenerator(
        name         = IdentifiedByIdEntity.ID_GENERATOR_NAME,
        sequenceName = "seq__materials"
)
@SecondaryTable(
        name          = "materials_used_quantities",
        pkJoinColumns = @PrimaryKeyJoinColumn(name = "id")
)
@EqualsAndHashCode(of = "code")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Material extends IdentifiedByIdEntity {

    @Getter
    @Column(unique = true, nullable = false)
    private String code;

    @Getter @Setter
    private String name;

    @Getter @Setter
    @Column(nullable = false)
    private boolean recyclable;

    @Getter
    @Immutable
    @ElementCollection
    @CollectionTable(
            name        = "materials_used_quantities_years",
            joinColumns = @JoinColumn(
                    name = "id"
            )
    )
    @MapKeyColumn(
            name       = "year",
            insertable = false,
            updatable  = false
    )
    @Column(
            name       = "quantity",
            updatable  = false,
            insertable = false
    )
    private Map<Integer, BigDecimal> yearUsedQuantities;

    @Getter
    @Column(
            name      = "quantity",
            table     = "materials_used_quantities",
            updatable = false
    )
    private BigDecimal usedQuantity;

    @OneToMany(
            mappedBy      = "material",
            cascade       = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Stock> stocks;

    {
        stocks = new HashSet<>();
    }

    public Material(String code) {
        this.code = code;
    }

    public Set<Stock> getStocks() {
        return new HashSet<>(stocks);
    }

    boolean addStock(Stock stock) {
        return stocks.add(stock);
    }
}