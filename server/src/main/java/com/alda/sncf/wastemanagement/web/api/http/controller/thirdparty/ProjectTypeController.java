package com.alda.sncf.wastemanagement.web.api.http.controller.thirdparty;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
@RestController("thirdPartyProjectTypeController")
@RequestMapping("/tp/projectypes")
public class ProjectTypeController {
}
