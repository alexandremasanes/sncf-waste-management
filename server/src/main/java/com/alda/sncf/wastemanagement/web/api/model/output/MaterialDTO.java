package com.alda.sncf.wastemanagement.web.api.model.output;

import com.alda.sncf.wastemanagement.business.mapping.entity.Material;
import com.alda.sncf.wastemanagement.web.api.model.common.StockId;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by alexandremasanes on 31/01/2018.
 */
@SuppressWarnings("all")
public class MaterialDTO extends DTO<Material> {

    private String name;

    private boolean recyclable;

    private Map<Integer, BigDecimal> yearUsedQuantities;

    private BigDecimal usedQuantity;

    @SerializedName("stockIds")
    private StockId[] stockIdDTOs;

    public MaterialDTO(Material material) {
        super(material);
        name = material.getName();
        recyclable = material.isRecyclable();
        yearUsedQuantities = material.getYearUsedQuantities();
        usedQuantity = material.getUsedQuantity();
    }
}