package com.alda.sncf.wastemanagement.web.api.model.input;

import com.alda.sncf.wastemanagement.business.logic.MaterialService;

import lombok.Getter;
import org.checkerframework.checker.nullness.qual.NonNull;

import javax.validation.constraints.Min;
import java.io.Serializable;
import java.math.BigDecimal;

public class StockUpdate implements MaterialService.StockUpdate, Serializable {

    @Getter
    @NonNull
    @Min(0)
    private BigDecimal quantity;
}
