package com.alda.sncf.wastemanagement.business.repository;

import com.albema.common.orm.repository.Finder;
import com.albema.common.orm.repository.Remover;
import com.albema.common.orm.repository.Saver;
import com.alda.sncf.wastemanagement.business.mapping.entity.Material;

public interface MaterialDAO extends Finder<Material>, Saver<Material>, Remover<Material> {

}
