package com.alda.sncf.wastemanagement.web.common.business.logic;

import com.albema.common.util.Hasher;
import com.alda.sncf.wastemanagement.business.logic.UserService;
import com.alda.sncf.wastemanagement.business.mapping.entity.Token;
import com.alda.sncf.wastemanagement.business.mapping.entity.user.User;
import com.alda.sncf.wastemanagement.business.repository.TokenDAO;
import com.alda.sncf.wastemanagement.business.repository.UserDAO;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

/**
 * Created by alexandremasanes on 01/02/2018.
 */
@Service
class UserServiceImpl implements UserService {

    private final UserDAO userDAO;

    private final TokenDAO tokenDAO;

    private final String hashSalt;

    private final int tokenLifeTime;

    @Override
    @Transactional(readOnly = true)
    public User getOne(long id) {
        return userDAO.find(id);
    }

    @Override
    @Transactional(readOnly = true)
    public User getOne(String emailAddress) {
        return userDAO.findByEmailAddress(emailAddress);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getAll() {
        return userDAO.find();
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> get(long... ids) {
        return userDAO.find(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public Token getOneToken(UUID value) {
        return tokenDAO.find(value);
    }

    @Override
    @Transactional
    public Token createToken(TokenCreation tokenCreation) throws NoSuchAlgorithmException {
        User  user;
        Token token;

        user = userDAO.findByEmailAddressAndHash(
                tokenCreation.getEmailAddress(),
                Hasher.hash(
                        tokenCreation.getEmailAddress() +
                                hashSalt +
                                tokenCreation.getPassword(),
                        Hasher.SHA_256
                )
        );

        if(user == null)
            return null;

        if(user.getToken() != null)
            tokenDAO.remove(user.getToken());


        token = new Token(user);

        userDAO.save(user);

        return token;
    }

    @Override
    @Transactional
    public void update(User user) {
        userDAO.save(user);
    }

    @Override
    @Transactional
    public void removeExpiredTokens() {
        tokenDAO.removeExpiredTokens(tokenLifeTime);
    }


    UserServiceImpl(
                                       UserDAO  userDAO,
                                       TokenDAO tokenDAO,
            @Value("${hashSalt}")      String   hashSalt,
            @Value("${tokenLifeTime}") int      tokenLifeTime
    ) {
        this.userDAO = userDAO;
        this.tokenDAO = tokenDAO;
        this.hashSalt = hashSalt;
        this.tokenLifeTime = tokenLifeTime;
    }
}