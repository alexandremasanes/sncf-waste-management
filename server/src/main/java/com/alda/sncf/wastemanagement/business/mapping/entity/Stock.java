package com.alda.sncf.wastemanagement.business.mapping.entity;

import com.albema.common.orm.entity.RecordableEntity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by alexandremasanes on 30/03/2018.
 */
@Entity
@Table(name = "stocks")
@SecondaryTable(
        name          = "stocks_used_quantities",
        pkJoinColumns = {
                @PrimaryKeyJoinColumn(name = "id"),
                @PrimaryKeyJoinColumn(name = "quality")
        }
)
@IdClass(Stock.Identifier.class)
@EqualsAndHashCode(of = {"material", "quality"})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Stock extends RecordableEntity {

    public enum Quality {
        BAD,
        AVERAGE,
        GOOD
    }

    @EqualsAndHashCode(of = {"material", "quality"})
    protected static class Identifier implements Serializable {

        private Material material;

        private Quality quality;
    }

    @Getter
    @Id
    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "id", referencedColumnName = "id")
    private Material material;

    @Getter
    @Id
    @Enumerated
    @Column(nullable = false)
    private Quality quality;

    @Getter @Setter
    private BigDecimal quantity;

    @OneToMany(
            mappedBy      = "stock",
            cascade       = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Composition> compositions;

    @Getter
    @Column(
            name       = "quantity",
            table      = "stocks_used_quantities",
            insertable = false,
            updatable  = false
    )
    private BigDecimal usedQuantity;

    {
        compositions = new HashSet<>();
    }

    public Stock(Material material, Quality quality) {
        this.material = material;
        this.quality  = quality;
        material.addStock(this);
    }

    public Set<Composition> getCompositions() {
        return new HashSet<>(compositions);
    }

    boolean addComposition(Composition composition) {
        return compositions.add(composition);
    }
}