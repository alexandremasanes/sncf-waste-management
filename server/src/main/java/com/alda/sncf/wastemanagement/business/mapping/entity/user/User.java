package com.alda.sncf.wastemanagement.business.mapping.entity.user;

import com.albema.common.orm.entity.identifiable.IdentifiedByIdEntity;
import com.alda.sncf.wastemanagement.business.mapping.entity.Token;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * Created by alexandremasanes on 30/01/2018.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(
        name         = IdentifiedByIdEntity.ID_GENERATOR_NAME,
        sequenceName = "seq__users"
)
@EqualsAndHashCode(of = "emailAddress")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class User extends IdentifiedByIdEntity {

    @Getter
    @Setter
    @Column(name = "last_name")
    private String lastName;

    @Getter
    @Setter
    @Column(name = "first_name")
    private String firstName;

    @Getter
    @Column(
            name      = "email_address",
            unique    = true,
            nullable  = false,
            updatable = false
    )
    private String emailAddress;

    @Getter
    @Setter
    private String hash;

    @Getter
    @Setter
    @OneToOne(
            mappedBy      = "user",
            fetch         = FetchType.LAZY,
            cascade       = CascadeType.ALL,
            orphanRemoval = true
    )
    private Token token;

    public User(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}