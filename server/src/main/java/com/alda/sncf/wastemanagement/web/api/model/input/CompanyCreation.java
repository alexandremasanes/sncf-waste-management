package com.alda.sncf.wastemanagement.web.api.model.input;

import com.albema.spring.validation.UniqueConstraints;
import com.albema.spring.validation.UniqueEntityProperty;
import com.alda.sncf.wastemanagement.business.logic.CompanyService;
import com.alda.sncf.wastemanagement.business.mapping.entity.Company;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@UniqueConstraints({
        @UniqueEntityProperty(
                entityClass    = Company.class,
                attribute = "name"
        ),
        @UniqueEntityProperty(
                entityClass    = Company.class,
                attribute = "sirenNumber"
        )
})
public class CompanyCreation implements CompanyService.CompanyCreation, Serializable {

    @Getter
    @NotEmpty
    private String name;

    @Getter
    @NotEmpty
    @UniqueEntityProperty(entityClass = Company.class)
    private String sirenNumber;
}
