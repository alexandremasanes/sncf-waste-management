package com.alda.sncf.wastemanagement.web.common.repository;

import com.albema.spring.repository.DAO;
import com.alda.sncf.wastemanagement.business.mapping.entity.Material;
import com.alda.sncf.wastemanagement.business.repository.MaterialDAO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
class MaterialDAOImpl extends DAO implements MaterialDAO {

    @Override
    public List<Material> find() {
        return find(Material.class);
    }

    @Override
    public List<Material> find(long... ids) {
        return find(Material.class, ids);
    }

    @Override
    public Material find(long id) {
        return find(Material.class, id);
    }

    @Override
    public boolean has(long id) {
        return false;
    }

    @Override
    public long getNextId() {
        return 0;
    }

    @Override
    public boolean remove(long... ids) {
        return remove(Material.class, ids);
    }

    @Override
    public boolean remove(long id) {
        return remove(Material.class, id);
    }

    @Override
    public boolean remove(Material company) {
        return remove0(company);
    }

    @Override
    public boolean remove(Material... company) {
        return remove0(company);
    }

    @Override
    public void save(Material entity) {
        save0(entity);
    }

    protected MaterialDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}