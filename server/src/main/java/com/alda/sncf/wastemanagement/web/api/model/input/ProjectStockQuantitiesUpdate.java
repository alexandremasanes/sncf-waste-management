package com.alda.sncf.wastemanagement.web.api.model.input;

import com.alda.sncf.wastemanagement.business.logic.ProjectService;
import com.alda.sncf.wastemanagement.web.api.model.common.StockIdQuantity;
import lombok.Getter;

import javax.validation.Valid;
import java.io.Serializable;

public class ProjectStockQuantitiesUpdate implements ProjectService.StockQuantitiesUpdate, Serializable {

    @Valid
    @Getter
    private StockIdQuantity[] stockIdQuantities;
}