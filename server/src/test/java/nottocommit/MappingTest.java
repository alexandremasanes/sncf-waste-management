package nottocommit;

import com.alda.sncf.wastemanagement.business.mapping.entity.*;
import com.alda.sncf.wastemanagement.business.repository.*;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath*:/applicationContext.xml")
@TestPropertySource("classpath:log4j.properties")
public class MappingTest {

    @Autowired
    private ProjectDAO projectDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private MaterialDAO materialDAO;

    @Autowired
    private ProjectTypeDAO projectTypeDAO;

    @Autowired
    private CompanyDAO companyDAO;

    @Autowired
    private CompositionDAO compositionDAO;


    @Test @Transactional
    public void test0() {

        Material material;

        material = new Material("ZNC");

        material.setName("zinc");

        material.setRecyclable(false);

        materialDAO.save(material);

        Stock stock;

        stock = new Stock(material, Stock.Quality.GOOD);

        stock.setQuantity(BigDecimal.valueOf(55555.55));

        stockDAO.save(stock);

        Project project;

        project = new Project(new Date(), projectTypeDAO.find(1), companyDAO.find(1));

        project.setDescription("ptdr");

        final Composition composition;

        composition = new Composition(stock, project);

        composition.setQuantity(BigDecimal.valueOf(25161.56));

        projectDAO.save(project);
    }

    @Test
    @Transactional(readOnly = true)
    public void test1() {
        projectDAO.findQuantitiesByMaterialAndCompany().forEach(
                v -> System.err.println(ToStringBuilder.reflectionToString(v))
        );
    }

    @Test
    @Transactional
    public void test2() {
        final Project project;

        project = projectDAO.find().get(0);

        project.getCompositions().stream().findFirst().get().setQuantity(BigDecimal.ZERO);
    }
}