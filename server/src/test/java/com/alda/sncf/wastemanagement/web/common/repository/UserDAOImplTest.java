package com.alda.sncf.wastemanagement.web.common.repository;

import com.alda.sncf.wastemanagement.business.mapping.entity.user.Manager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by alexandremasanes on 01/02/2018.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/applicationContext.xml")
public class UserDAOImplTest {

    private static final Logger logger = LoggerFactory.getLogger(UserDAOImplTest.class);

    @Autowired
    private UserDAOImpl userDAO;

    @Test
    @Transactional(readOnly = true)
    public void find() throws Exception {
    }

    @Test
    @Transactional(readOnly = true)
    public void find1() throws Exception {

    }

    @Test
    @Transactional(readOnly = true)
    public void find2() throws Exception {

    }

    @Test
    @Transactional(readOnly = true)
    public void findByEmailAddressAndHash() throws Exception {

    }

    @Test
    @Transactional(readOnly = true)
    public void findByEmailAddress() throws Exception {

    }

    @Test
    @Transactional(readOnly = true)
    public void has() throws Exception {

    }

    @Test
    @Transactional(readOnly = true)
    public void getNextId() throws Exception {

    }

    @Test
    @Transactional
    public void remove() throws Exception {

    }

    @Test
    @Transactional
    public void remove1() throws Exception {

    }

    @Test @Transactional
    public void remove2() throws Exception {

    }

    @Test @Transactional
    public void remove3() throws Exception {

    }

    @Test @Transactional
    public void save() throws Exception {
        Manager manager;

        manager = new Manager("toto.toto@gmail.fr");

        manager.setFirstName("toto");

        manager.setLastName("toto");

        manager.setHash("toto");

        System.err.println(manager.getId());
    }
}